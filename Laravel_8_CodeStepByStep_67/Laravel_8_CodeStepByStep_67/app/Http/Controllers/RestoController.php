<?php

namespace App\Http\Controllers;

use App\Restaurant;
use Illuminate\Http\Request;

class RestoController extends Controller
{
    // function index()
    // {
    //     return "Test";
    // }

    function index()
    {
        return view('home');
    }

    function list()
    {
        $data = Restaurant::all();

        return view('list', ['data' => $data]);
    }

    function add(Request $request)
    {
        return $request;
    }
}
