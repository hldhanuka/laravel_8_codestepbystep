<?php

namespace App\Http\Controllers;

use App\Restaurant;
use Illuminate\Http\Request;
use SebastianBergmann\GlobalState\Restorer;

class RestoController extends Controller
{
    // function index()
    // {
    //     return "Test";
    // }

    function index()
    {
        return view('home');
    }

    function list()
    {
        $data = Restaurant::all();

        return view('list', ['data' => $data]);
    }

    function add(Request $request)
    {
        $resto = new Restaurant();

        $resto->name = $request->input('name');
        $resto->email = $request->input('email');
        $resto->address = $request->input('address');

        $resto->save();

        // $request->session()->flush('status', 'Resturant entered Successfully');
        // Session::flush('status', 'Resturant entered Successfully');

        return redirect('list')->with('status', 'Resturant entered Successfully');
    }
}
