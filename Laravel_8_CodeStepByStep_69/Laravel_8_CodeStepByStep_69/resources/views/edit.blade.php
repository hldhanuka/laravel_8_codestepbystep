@extends('layout')

@section('content')

<h1> Edit Restaurant</h1>

<div class="col-sm-6">
    <form method="post" action="/edit/{{$data->id}}">
        {{ method_field('PUT') }}
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" value="{{$data->name}}" name="name" placeholder="Enter name">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" value="{{$data->email}}" name="email" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="address">Address</label>
            <input type="text" class="form-control" value="{{$data->address}}" name="address" placeholder="Enter address">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

@stop